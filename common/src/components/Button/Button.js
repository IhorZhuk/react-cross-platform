import React from 'react';
import {Button} from 'react-native';
// import styled from 'styled-components';

// const ButtonWrapper = styled.button`
// 	border-radius: 8px;
// 	color: #fff;
// 	background: mediumvioletred;
// 	padding: 8px 15px;
// 	border: none;
// 	outline: none;
// `;

const Btn = (props) => {
  return <Button title={props.title} color="#841584" onPress={props.onPress}/>
}

export default Btn;
