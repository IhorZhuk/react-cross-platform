const path = require('path');


var config = (env, argv) => {

  let nativeName = path.resolve('node_modules/react-native');
  if (env.type == 'web') {
    nativeName = path.resolve('node_modules/react-native-web');
  }

  return {
    entry: './src/index.js',
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
          },
        },
        {
          test: /\.scss$/,
          use: [
            { loader: 'style-loader' },
            { loader: 'css-loader' },
            { loader: 'sass-loader' },
          ],
        },
        {
          test: /\.(png|gif|jpg|svg)$/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 50000,
            },
          },
        },
      ],
    },
    resolve: {
      extensions: ['.scss', '.js', '.json', '.png', '.gif', '.jpg', '.svg'],
      alias: {
        'react-native': nativeName,
      }
    },
    output: {
      path: path.resolve(__dirname, 'dist/'),
      publicPath: '',
      filename: 'common.js',
      // This field determines how things are importable when installed from other
      // sources. UMD may not be correct now and there is an open issue to fix this,
      // but until then, more reading can be found here:
      // https://webpack.js.org/configuration/output/#output-librarytarget
      libraryTarget: 'umd',
    },
  }
}
module.exports = config;
