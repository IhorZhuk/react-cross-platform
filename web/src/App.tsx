import React from 'react';
import logo from './logo.svg';
import './App.css';
import * as Common from './../../common/dist/common.js';

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Common.Button title="Test" onPress={()=> console.log('sdf')}/>
      </header>
    </div>
  );
}

export default App;
